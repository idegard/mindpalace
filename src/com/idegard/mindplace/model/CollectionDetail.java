package com.idegard.mindplace.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "collectiondetail")
public class CollectionDetail implements java.io.Serializable {

	private Integer clave;
	private Room room;
	private Imagen imagen;
	private String nombre;
	private Collection coleccion;
	private RoomData roomData;

	public CollectionDetail() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", nullable = false)
	public Integer getClave() {
		return this.clave;
	}

	public void setClave(Integer clave) {
		this.clave = clave;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "room", nullable = false)
	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "image", nullable = false)
	public Imagen getImagen() {
		return imagen;
	}

	public void setImagen(Imagen imagen) {
		this.imagen = imagen;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "collection", nullable = false)
	public Collection getColeccion() {
		return coleccion;
	}

	public void setColeccion(Collection coleccion) {
		this.coleccion = coleccion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roomdatacord", nullable = false)
	public RoomData getRoomData() {
		return roomData;
	}

	public void setRoomData(RoomData roomData) {
		this.roomData = roomData;
	}
	
	
	
	
}
