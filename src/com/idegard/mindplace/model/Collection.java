package com.idegard.mindplace.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "collection")
public class Collection implements java.io.Serializable {

	private Integer clave;
	private String nombre;
	private String usuario;
	private Set<CollectionDetail> detalles;

	public Collection() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", nullable = false)
	public Integer getClave() {
		return this.clave;
	}

	public void setClave(Integer clave) {
		this.clave = clave;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "usuario")
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="collection")
	public Set<CollectionDetail> getDetalles() {
		return detalles;
	}

	public void setDetalles(Set<CollectionDetail> detalles) {
		this.detalles = detalles;
	}

	
	
	
	
	
	
}
