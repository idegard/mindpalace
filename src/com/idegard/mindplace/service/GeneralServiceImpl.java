package com.idegard.mindplace.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.idegard.mindplace.model.Collection;
import com.idegard.mindplace.model.CollectionDetail;
import com.idegard.mindplace.model.Imagen;
import com.idegard.mindplace.repository.CollectionRepository;
import com.idegard.mindplace.repository.ImagenRepository;

@Service
public class GeneralServiceImpl implements GeneralService {

	@Autowired
	private CollectionRepository collectionRepository;
	@Autowired
	private ImagenRepository imagenRepository;
	
	@Override
	public ArrayList<Collection> getColecciones() {
		// TODO Auto-generated method stub
		return collectionRepository.find();
	}

	@Override
	public Collection getColleccion(String nombre) {
		// TODO Auto-generated method stub
		if(nombre.equals("random")){
			return getRandom();
		}
		else{
			return collectionRepository.find(nombre);	
		}
	}

	private Collection getRandom() {
		// TODO Auto-generated method stub
		Collection c = collectionRepository.find(1);
		for(CollectionDetail d:c.getDetalles()){
			d.setImagen(imagenRepository.random());
		}
		return c;
	}

	@Override
	public Collection getColleccion(Integer clave) {
		// TODO Auto-generated method stub
		return collectionRepository.find(clave);
	}

	@Override
	public Imagen getImagen(Integer clave) {
		// TODO Auto-generated method stub
		return imagenRepository.find(clave);
	}

}
