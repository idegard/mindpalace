package com.idegard.mindplace.service;

import java.util.ArrayList;

import com.idegard.mindplace.model.Collection;
import com.idegard.mindplace.model.Imagen;

public interface GeneralService {

	ArrayList<Collection> getColecciones();
	
	Collection getColleccion(String nombre);
	Collection getColleccion(Integer clave);
	
	Imagen getImagen(Integer clave);
	
}
