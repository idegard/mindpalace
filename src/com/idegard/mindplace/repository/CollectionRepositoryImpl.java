package com.idegard.mindplace.repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 * JPA implementation of the {@link ImagenRepository} interface.
 *
 
 * @author Idegard
 */
@Repository
public class CollectionRepositoryImpl implements CollectionRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
	public com.idegard.mindplace.model.Collection find(String nombre) {
		// TODO Auto-generated method stub
    	 Query query = this.em.createQuery("SELECT collection FROM Collection collection WHERE collection.nombre = ? OR collection.usuario = ?");
		    query.setParameter(1, nombre);
		    query.setParameter(2, nombre);
		    return ((List<com.idegard.mindplace.model.Collection>)query.getResultList()).get(0);
	}
	@Override
	public com.idegard.mindplace.model.Collection find(Integer clave) {
		// TODO Auto-generated method stub
		return em.find(com.idegard.mindplace.model.Collection.class, clave);
	}
	@Override
	public ArrayList<com.idegard.mindplace.model.Collection> find() {
		// TODO Auto-generated method stub
		Query query = this.em.createQuery("SELECT collection FROM Collection collection");
	    return (ArrayList<com.idegard.mindplace.model.Collection>)query.getResultList();
	}	
	}


   

