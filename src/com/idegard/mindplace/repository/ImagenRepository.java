package com.idegard.mindplace.repository;


import com.idegard.mindplace.model.Imagen;

public interface ImagenRepository {
	
	Imagen find(Integer clave);

	Imagen random();
}
