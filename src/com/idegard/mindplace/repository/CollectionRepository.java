package com.idegard.mindplace.repository;


import java.util.ArrayList;
import com.idegard.mindplace.model.Collection;

public interface CollectionRepository {
	
	ArrayList<Collection> find();

	Collection find(String nombre);
	
	Collection find(Integer clave);
}
