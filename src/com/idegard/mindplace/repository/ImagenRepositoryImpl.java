package com.idegard.mindplace.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.idegard.mindplace.model.Imagen;

/**
 * JPA implementation of the {@link ImagenRepository} interface.
 *
 
 * @author Idegard
 */
@Repository
public class ImagenRepositoryImpl implements ImagenRepository {

    @PersistenceContext
    private EntityManager em;

	@Override
	public Imagen find(Integer clave) {
		// TODO Auto-generated method stub
		return em.find(Imagen.class, clave);
	}
	
	@Override
	public Imagen random() {
		// TODO Auto-generated method stub
		return (Imagen)this.em.createQuery("SELECT imagen FROM Imagen imagen order by rand()")
				.setMaxResults(1).getSingleResult();
	}
	

   
	}


   

