package com.idegard.mindpalace.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import javax.imageio.ImageIO;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.idegard.mindpalace.utils.MergeImage;
import com.idegard.mindplace.model.Collection;
import com.idegard.mindplace.model.CollectionDetail;
import com.idegard.mindplace.model.Imagen;
import com.idegard.mindplace.service.GeneralService;

@Component
@Path("/collection")
public class MindPalaceService {
	
	@Autowired
	private GeneralService servicio;
	private HashMap<Integer,JSONObject> datosTemporales = new HashMap<>();

		@GET
		@Path("/{j}/{k}/{l}")
		@Produces("image/jpg")
		public Response responseImg( @PathParam("j") String nombre,@PathParam("k") int numero,@PathParam("l") int ruid ) {
			 try {
				 Collection coleccion = servicio.getColleccion(nombre);
				 InputStream in = new ByteArrayInputStream(coleccion.getDetalles().iterator().next().getRoom().getImagen());
				BufferedImage imagen = ImageIO.read(in);
				 MergeImage mergeImage = new MergeImage(imagen);
				 JSONObject o = new JSONObject();
				 int contador=0;
				 ArrayList<CollectionDetail> listaDetalles = new ArrayList<CollectionDetail>(coleccion.getDetalles());
				 java.util.Collections.sort(listaDetalles,new Comparator<CollectionDetail>(){

					@Override
					public int compare(CollectionDetail o1, CollectionDetail o2) {
						// TODO Auto-generated method stub
						return o1.getRoomData().getPosx().compareTo(o2.getRoomData().getPosx());
					}});
				 for(CollectionDetail detalle:listaDetalles){
					 InputStream input = new ByteArrayInputStream(detalle.getImagen().getImagen());
					 BufferedImage imagenAgregar = ImageIO.read(input);
					 mergeImage.agregarImagen(imagenAgregar, detalle.getRoomData().getPosx(), detalle.getRoomData().getPosy(),
							 detalle.getRoomData().getWidth(), detalle.getRoomData().getHeight());
					 o.put(String.valueOf(contador++),detalle.getImagen().getClave());
					 
				 }
				 datosTemporales.put(ruid, o);
				 ByteArrayOutputStream os = new ByteArrayOutputStream();
				 ImageIO.write(mergeImage.merge(), "jpg", os);
				 InputStream is = new ByteArrayInputStream(os.toByteArray());
					  return Response.ok(is,MediaType.IMAGE_JPEG_VALUE).build();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  
			 	return Response.serverError().build();
		}
		@GET
		@Path("/data/{j}/{k}")
		@Produces("application/json") 
		public String responseMsg( @PathParam("j") String nombre,@PathParam("k") int numero ) {
			 JSONObject o = new JSONObject();
			 Collection coleccion = servicio.getColleccion(nombre);
			 int c = 0;
			 for(CollectionDetail detalle:coleccion.getDetalles()){
				 o.put(String.valueOf(c), detalle.getImagen().getClave());
				 c++;
			 }
				  return o.toString();
		}
		
		@GET
		@Path("/datos/{j}")
		@Produces("application/json") 
		public String responseMsg( @PathParam("j")int numero ) {
				JSONObject o=datosTemporales.remove(numero);
				  return o!=null?o.toString():"";
		}
		
		@GET
		@Path("/img/{k}")
		@Produces("image/jpg")
		public Response responseImgClave(@PathParam("k") int numero ) {
			 try {
				Imagen imagen = servicio.getImagen(numero);
				 InputStream in = new ByteArrayInputStream(imagen.getImagen());
				BufferedImage img = ImageIO.read(in);
				 return Response.ok(img,MediaType.IMAGE_JPEG_VALUE).build(); 
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  
			 	return Response.serverError().build();
		}

}

