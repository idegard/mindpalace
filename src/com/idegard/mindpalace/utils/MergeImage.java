package com.idegard.mindpalace.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class MergeImage {
	BufferedImage background;
	ArrayList<BufferedImage> images = new ArrayList<BufferedImage>();
	ArrayList<Integer[]> coordenadas = new ArrayList<Integer[]>();
	
	public MergeImage(BufferedImage imagen){
		background = imagen;
	}
	
	public void agregarImagen(BufferedImage imagen, int x,int y,int ancho,int alto){
		images.add(imagen);
		coordenadas.add(new Integer[]{x,y,ancho,alto});
	}
	
	public BufferedImage merge(){
		 for(int i=0;i<coordenadas.size();i++){
			Integer[] coord = coordenadas.get(i);
			BufferedImage ima = images.get(i);
			BufferedImage resizedImage = new BufferedImage(coord[2], coord[3],ima.getType());
			Graphics2D g = resizedImage.createGraphics();
			g.drawImage(ima, 0, 0, coord[2], coord[3], null);
			g.dispose();
			background.createGraphics().drawImage(resizedImage, coord[0], coord[1], null);
		 }
		return background;
	}
	
}
